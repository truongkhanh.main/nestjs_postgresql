import { entityProviders } from './entity.providers';
import { EntityRepository } from './entity.repository';
import { Module } from '@nestjs/common';
import { EntityController } from './entity.controller';
import { EntityService } from './entity.service';

@Module({
  controllers: [EntityController],
  providers: [...entityProviders, EntityRepository, EntityService],
  exports: [EntityService],
})
export class EntityModule {}
