import { DataSource } from 'typeorm';
import { ENTITY_CONST } from './constants/entity.constant';
import { EntityEntity } from './entities/entity.entity';

export const entityProviders = [
  {
    provide: ENTITY_CONST.MODEL_PROVIDER,
    useFactory: (connection: DataSource) => connection.getRepository(EntityEntity),
    inject: ['DATA_SOURCE'],
  },
];
