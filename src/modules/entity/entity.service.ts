import { CreateEntityDto } from './dto';
import { Injectable, HttpStatus } from '@nestjs/common';
import { EntityRepository } from './entity.repository';

@Injectable()
export class EntityService {
  constructor(private entityRepository: EntityRepository) {}

  healthCheck() {
    return {
      message: 'OK Test',
      statusCode: HttpStatus.OK,
    };
  }

  getList() {
    return this.entityRepository.findPaginate();
  }

  create(data: CreateEntityDto) {
    const entity = this.entityRepository.create(data);
    return this.entityRepository.save(entity);
  }
}
