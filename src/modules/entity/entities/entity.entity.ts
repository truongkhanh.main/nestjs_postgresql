import { BaseEntity } from '../../../database/base/entity.base';
import { Column, Entity } from 'typeorm';
import { ENTITY_CONST } from '../constants/entity.constant';

@Entity({ name: ENTITY_CONST.MODEL_NAME })
export class EntityEntity extends BaseEntity {
  @Column()
  name: string;
}
