import { Body, Controller, Get, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CreateEntityDto } from './dto';
import { SWAGGER_RESPONSE } from './constants/entity.constant';
import { EntityService } from './entity.service';

@ApiTags('Entity')
@Controller({
  version: ['1'],
  path: 'entities',
})
export class EntityController {
  constructor(private readonly entityService: EntityService) {}

  @ApiOkResponse(SWAGGER_RESPONSE.GET_ENTITIES_SUCCESS)
  @Get()
  getList() {
    return this.entityService.getList();
  }

  @ApiCreatedResponse(SWAGGER_RESPONSE.CREATE_ENTITY_SUCCESS)
  @Post()
  create(@Body() body: CreateEntityDto) {
    return this.entityService.create(body);
  }

  @ApiOkResponse(SWAGGER_RESPONSE.HEALTH_CHECK)
  @Get('health-check')
  healthCheck() {
    return this.entityService.healthCheck();
  }
}
