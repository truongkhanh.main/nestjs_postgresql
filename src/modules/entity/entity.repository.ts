import { Inject, Injectable } from '@nestjs/common';
import { BaseRepository } from '../../database/base/repository.base';
import { Repository } from 'typeorm';
import { ENTITY_CONST } from './constants/entity.constant';
import { EntityEntity } from './entities/entity.entity';

@Injectable()
export class EntityRepository extends BaseRepository<EntityEntity> {
  constructor(
    @Inject(ENTITY_CONST.MODEL_PROVIDER)
    entityRepository: Repository<EntityEntity>,
  ) {
    super(entityRepository);
  }
}
