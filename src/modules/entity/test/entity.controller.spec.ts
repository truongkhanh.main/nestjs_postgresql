import { EntityService } from './../entity.service';
import { EntityController } from './../entity.controller';
import { DeepPartial } from 'typeorm';
import { EntityEntity } from '../entities/entity.entity';
import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus } from '@nestjs/common';
import { CreateEntityDto } from '../dto';

describe('EntityController', () => {
  let controller: EntityController;
  let service: EntityService;

  // Mock data
  const entities: DeepPartial<EntityEntity>[] = [
    {
      id: 1,
      name: 'Entity 1',
    },
    {
      id: 2,
      name: 'Entity 2',
    },
  ];

  // Mock dependencies
  const mockEntityService = {
    getList: jest.fn().mockResolvedValue({
      data: entities,
      page: 1,
      pageSize: 20,
      totalPage: 1,
      totalItem: entities.length,
    }),
    create: jest.fn().mockImplementation(async (data: CreateEntityDto) => {
      const entity = new EntityEntity();
      for (const key in data) {
        entity[key] = data[key];
      }
      return entity;
    }),
    healthCheck: jest.fn().mockReturnValue({
      message: 'OK Test',
      statusCode: HttpStatus.OK,
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [EntityController],
      providers: [
        {
          provide: EntityService,
          useValue: mockEntityService,
        },
      ],
    }).compile();

    controller = module.get<EntityController>(EntityController);
    service = module.get<EntityService>(EntityService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('healthCheck() unit testing', () => {
    it('should have been called EntityService.healthCheck', () => {
      controller.healthCheck();
      expect(service.healthCheck).toHaveBeenCalled();
    });
    it('should take status code 200', () => {
      const result = controller.healthCheck();
      expect(result.statusCode).toEqual(HttpStatus.OK);
      expect(result.message).toEqual(expect.any(String));
    });
  });

  describe('getList() unit testing', () => {
    it('should have been called EntityService.getList', async () => {
      await controller.getList();
      expect(service.getList).toHaveBeenCalled();
    });
    it('should take list pagination of entity', async () => {
      const result = await controller.getList();
      expect(result.data).toEqual(entities);
    });
  });

  describe('create() unit testing', () => {
    const createEntityDto: CreateEntityDto = { name: 'Entity 3' };
    it('should have been called EntityService.create', async () => {
      await controller.create(createEntityDto);
      expect(service.create).toHaveBeenCalledWith(createEntityDto);
    });
    it('should take created entity', async () => {
      const result = await controller.create(createEntityDto);
      expect(result).toBeInstanceOf(EntityEntity);
      expect(result.name).toEqual(createEntityDto.name);
    });
  });
});
