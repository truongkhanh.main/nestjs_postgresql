import { EntityEntity } from './../entities/entity.entity';
import { HttpStatus } from '@nestjs/common';
import { EntityRepository } from './../entity.repository';
import { Test, TestingModule } from '@nestjs/testing';
import { EntityService } from '../entity.service';
import { DeepPartial } from 'typeorm';
import { IPagination } from 'src/database/interfaces/pagination.interface';
import { CreateEntityDto } from '../dto';

describe('EntityService', () => {
  let service: EntityService;

  // Mock data
  const entities: DeepPartial<EntityEntity>[] = [
    {
      id: 1,
      name: 'Entity 1',
    },
    {
      id: 2,
      name: 'Entity 2',
    },
  ];

  // Mock dependencies
  const mockEntityRepository = {
    create: jest.fn().mockImplementation((data: DeepPartial<EntityEntity>) => {
      const entity = new EntityEntity();
      for (const key in data) {
        entity[key] = data[key];
      }
      return entity;
    }),
    save: jest.fn().mockImplementation(async (data: EntityEntity) => data),
    findPaginate: jest.fn().mockImplementation(async (): Promise<IPagination<any>> => {
      return {
        data: entities,
        page: 1,
        pageSize: 20,
        totalPage: 1,
        totalItem: entities.length,
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        EntityService,
        {
          provide: EntityRepository,
          useValue: mockEntityRepository,
        },
      ],
    }).compile();

    service = module.get<EntityService>(EntityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('healthCheck() unit testing', () => {
    it('should take status code 200', async () => {
      const result = service.healthCheck();
      expect(result.statusCode).toEqual(HttpStatus.OK);
      expect(result.message).toEqual(expect.any(String));
    });
  });

  describe('getList() unit testing', () => {
    it('should take list pagination of entity', async () => {
      const result = await service.getList();
      expect(result.data).toEqual(entities);
    });
  });

  describe('create() unit testing', () => {
    const createEntityDto: CreateEntityDto = { name: 'Entity 3' };
    it('should take created entity', async () => {
      const result = await service.create(createEntityDto);
      expect(result).toBeInstanceOf(EntityEntity);
      expect(result.name).toEqual(createEntityDto.name);
    });
  });
});
