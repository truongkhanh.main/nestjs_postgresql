import { swaggerSchemaExample } from '../../../common/utils/swagger.util';

export const ENTITY_CONST = {
  MODEL_NAME: 'entities',
  MODEL_PROVIDER: 'ENTITY_MODEL',
};

export const SWAGGER_RESPONSE = {
  HEALTH_CHECK: swaggerSchemaExample({
    example: {
      message: 'OK Test',
      statusCode: 200,
    },
    description: 'API for health check',
  }),

  GET_ENTITIES_SUCCESS: swaggerSchemaExample({
    example: {
      data: [
        {
          id: 1,
          created_at: '2022-08-23T02:21:16.992Z',
          updated_at: '2022-08-23T02:21:16.992Z',
          deleted_at: null,
          name: 'Entity',
        },
      ],
      page: 1,
      pageSize: 20,
      totalPage: 1,
      totalItem: 1,
    },
    description: 'Get entities success',
  }),

  CREATE_ENTITY_SUCCESS: swaggerSchemaExample({
    example: {
      name: 'entity',
      deleted_at: null,
      id: 1,
      created_at: '2022-10-03T01:50:55.063Z',
      updated_at: '2022-10-03T01:50:55.063Z',
    },
    description: 'Create entity success',
  }),
};
