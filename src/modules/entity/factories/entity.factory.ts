import { setSeederFactory } from 'typeorm-extension';
import { EntityEntity } from '../entities/entity.entity';
import { Faker } from '@faker-js/faker';

export default setSeederFactory(EntityEntity, (faker: Faker) => {
  const entity = new EntityEntity();
  entity.name = faker.lorem.word();

  return entity;
});
