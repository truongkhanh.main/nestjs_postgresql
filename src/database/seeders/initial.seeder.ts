import { Seeder, SeederFactoryManager } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { EntityEntity } from '../../modules/entity/entities/entity.entity';

export default class InitialSeeder implements Seeder {
  public async run(dataSource: DataSource, factoryManager: SeederFactoryManager): Promise<any> {
    const entityFactory = factoryManager.get(EntityEntity);

    // save 10 factory generated entities, to the database
    await entityFactory.saveMany(10);
  }
}
